from PyQt6.QtCore import *
from PyQt6.QtWidgets import *
from PyQt6.QtWebEngineWidgets import *


class MainWindow(QMainWindow):
    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)

        self.browser = QWebEngineView()
        self.browser.setUrl(QUrl('http://localhost/summary.html'))

        self.setCentralWidget(self.browser)
        self.setWindowTitle('Web calculator')

        self.showMaximized()


if __name__ == '__main__':
    app = QApplication([])
    window = MainWindow()
    app.exec()
